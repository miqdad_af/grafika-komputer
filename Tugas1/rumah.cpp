#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    	glfwSetWindowShouldClose(window, GL_TRUE); // close program on ESC key
}
float speed = 0;

void setup_viewport(GLFWwindow* window)
{
	// setting viewports size, projection etc
	float ratio;
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	ratio = width / (float) height;
	glViewport(0, 0, width, height);

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1024, 1024, -650,650, 1.f, -1.f); // kiri kanan bawah atas
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void circle(float size)
{
     int N = 20;
     float pX, pY;
     glBegin(GL_POLYGON);
         for(int i = 0; i < N; i++)
         {
			 glColor3ub(i*0/N, i*255/N, i*0/N);
             pX = sin(i*2*3.14 / N) ;
             pY = cos(i*2*3.14 / N) ;
             glVertex2f(pX * size, pY * size);
         }
     glEnd();
}

void display()
{
	glPushMatrix();
		glTranslatef(-360,-550,0);
			glBegin(GL_POLYGON);
		  
				glColor3ub(185,185,185);
				glVertex3f(157.86f, 6.43f, 0.f);
				glVertex3f(225.71f, 26.79f, 0.f);
				glVertex3f(225.71f, 327.50f, 0.f);
				glVertex3f(150.71f, 315.36f, 0.f);
			
			glEnd();
			
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(155,155,155);
				glVertex3f(157.86f, 6.43f, 0.f);
				glVertex3f(147.14f,13.57f,0.f);
				glVertex3f(138.57f,319.64f,0.f);
				glVertex3f(150.36f,314.29f,0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(226.50f, 50.71f, 0.f);
				glVertex3f(415.71f, 110.71f, 0.f);
				glVertex3f(415.71f, 358.57f, 0.f);
				glVertex3f(226.50f, 318.93f, 0.f);
					
			glEnd();
			
		//tembok samping paling belakang warna coklat
			glBegin(GL_POLYGON);
		  
				glColor3ub(132,119,106);
				glVertex3f(-17.86f, 183.57f, 0.f);
				glVertex3f(-34.29f, 692.14f, 0.f);
				glVertex3f(-135.71f,707.86f, 0.f);
				glVertex3f(-112.86f,252.86f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(-1.43f, 399.29f, 0.f);
				glVertex3f(-12.86f,685.00f, 0.f);
				glVertex3f(-34.29f,688.57f, 0.f);
				glVertex3f(-25.86f,410.43f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(11.43f,411.43f, 0.f);
				glVertex3f(-2.14f,407.86f, 0.f);
				glVertex3f(-12.86f,685.00f, 0.f);
				glVertex3f(2.14f,687.86f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(-111.43f,251.43f, 0.f);
				glVertex3f(-126.43f,248.57f, 0.f);
				glVertex3f(-149.29f,707.14f, 0.f);
				glVertex3f(-135.71f,707.86f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(-126.43f,248.57f, 0.f);
				glVertex3f(-140.71f,260.00f, 0.f);
				glVertex3f(-164.29f,706.43f, 0.f);
				glVertex3f(-150.71f,707.14f, 0.f);
					
			glEnd();
			
				glEnd();
		//lantai atas kiri
			glBegin(GL_POLYGON);
		  
				glColor3ub(88,61,35);
				glVertex3f(12.86f, 410.71f, 0.f);
				glVertex3f(195.71f, 327.14f, 0.f);
				glVertex3f(191.43f,659.29f, 0.f);
				glVertex3f(3.57f,686.43f, 0.f);
					
			glEnd();
			
		//jendela paling kiri
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(47.86f, 472.14f, 0.f);
				glVertex3f(62.14f, 473.57f, 0.f);
				glVertex3f(58.57f, 614.29f, 0.f);
				glVertex3f(42.14f, 615.00f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				
				glVertex3f(42.14f, 615.00f, 0.f);
				glVertex3f(47.86f, 472.14f, 0.f);
				glVertex3f(3.57f, 487.86f, 0.f);
				glVertex3f(0.00f, 625.00f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				
				glVertex3f(8.57f, 492.86f, 0.f);
				glVertex3f(42.86f,481.43f, 0.f);
				glVertex3f(38.57f,606.43f, 0.f);
				glVertex3f(4.29f, 614.29f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(137,137,137);
				
				glVertex3f(8.57f, 492.86f, 0.f);
				glVertex3f(42.86f,481.43f, 0.f);
				glVertex3f(42.86f,489.29f, 0.f);
				glVertex3f(23.57f,495.71f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(132,119,106);
				glVertex3f(23.57f,495.71f, 0.f);
				glVertex3f(19.29f,610.00f, 0.f);
				glVertex3f(39.29f,606.43f, 0.f);
				glVertex3f(42.86f,489.29f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(21,21,21);
				glVertex3f(28.57f,503.57f, 0.f);
				glVertex3f(42.86f,497.86f, 0.f);
				glVertex3f(38.57f,601.43f, 0.f);
				glVertex3f(25.00f,605.00f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(106,177,177);
				glVertex3f(33.57f,508.57f, 0.f);
				glVertex3f(41.43f,505.00f, 0.f);
				glVertex3f(39.29f,597.14f, 0.f);
				glVertex3f(31.43f,598.57f, 0.f);
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(137,137,137);
				glVertex3f(41.43f,614.29f, 0.f);
				glVertex3f(0.71f,623.57f, 0.f);
				glVertex3f(15.00f,624.29f, 0.f);
				glVertex3f(57.86f,616.43f, 0.f);
					
			glEnd();
		
		//batasjendela
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(155,155,155);
				glVertex3f(-17.86f,640.71f, 0.f);
				glVertex3f(176.43f,601.43f, 0.f);
				glVertex3f(176.43f,621.43f, 0.f);
				glVertex3f(-18.57f,658.57f, 0.f);
					
			glEnd();
		
		//temboktiangabu2palingkananltatas
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(155,155,155);
				glVertex3f(847.14f, 970.00f, 0.f);
				glVertex3f(821.43f, 965.71f, 0.f);		
				glVertex3f(807.06f, 475.00f, 0.f);
				glVertex3f(824.29f, 469.29f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(185,185,185);
				glVertex3f(847.14f, 970.00f, 0.f);
				glVertex3f(863.57f, 967.14f, 0.f);		
				glVertex3f(840.00f, 473.57f, 0.f);
				glVertex3f(824.29f, 469.29f, 0.f);
			
			glEnd();
			
		//temboklt2dan3kananputih
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(547.86f, 430.71f, 0.f);
				glVertex3f(547.86f, 1037.14f, 0.f);		
				glVertex3f(834.29f, 1007.86f, 0.f);
				glVertex3f(812.86f, 478.57f, 0.f);
			
			glEnd();
		//jendelalt3kanan
		//kusen
			glBegin(GL_POLYGON);
		  
				glColor3ub(27,27,27);
				glVertex3f(581.43f, 695.71f, 0.f);
				glVertex3f(675.71f, 700.71f, 0.f);		
				glVertex3f(680.00f, 875.00f, 0.f);
				glVertex3f(585.71f, 877.14f, 0.f);
			
			glEnd();
		//kaca
			glBegin(GL_POLYGON);
		  
				glColor3ub(126,211,211);
				glVertex3f(580.71f, 700.86f, 0.f);
				glVertex3f(670.00f, 705.29f, 0.f);		
				glVertex3f(674.29f, 871.71f, 0.f);
				glVertex3f(585.71f, 871.86f, 0.f);
			
			glEnd();
		//sekathorizontal
			glBegin(GL_POLYGON);
		  
				glColor3ub(27,27,27);
				glVertex3f(582.86f, 750.71f, 0.f);
				glVertex3f(582.86f, 755.71f, 0.f);		
				glVertex3f(673.57f, 757.86f, 0.f);
				glVertex3f(673.57f, 753.57f, 0.f);
			
			glEnd();
		//sekatvertikal
			glBegin(GL_POLYGON);
		  
				glColor3ub(27,27,27);
				glVertex3f(616.43f, 755.71f, 0.f);
				glVertex3f(620.00f, 755.71f, 0.f);		
				glVertex3f(622.86f, 875.00f, 0.f);
				glVertex3f(617.86f, 875.00f, 0.f);
			
			glEnd();
		
		//jendelalt2kanan
		//kusen
			glBegin(GL_POLYGON);
		  
				glColor3ub(27,27,27);
				glVertex3f(577.14f, 500.71f, 0.f);
				glVertex3f(670.71f, 515.71f, 0.f);		
				glVertex3f(675.00f, 657.14f, 0.f);
				glVertex3f(579.43f, 649.29f, 0.f);
			
			glEnd();
		//kaca
			glBegin(GL_POLYGON);
		  
				glColor3ub(126,211,211);
				glVertex3f(580.00f, 644.29f, 0.f);
				glVertex3f(667.86f, 651.43f, 0.f);		
				glVertex3f(664.29f, 520.00f, 0.f);
				glVertex3f(577.86f, 507.14f, 0.f);
			
			glEnd();
		//sekatvertikal
			glBegin(GL_POLYGON);
		  
				glColor3ub(27,27,27);
				glVertex3f(609.29f, 510.00f, 0.f);
				glVertex3f(613.57f, 512.14f, 0.f);		
				glVertex3f(615.00f, 606.43f, 0.f);
				glVertex3f(612.14f, 604.29f, 0.f);
			
			glEnd();
		//sekathorizontal
			glBegin(GL_POLYGON);
		  
				glColor3ub(27,27,27);
				glVertex3f(577.14f, 600.00f, 0.f);
				glVertex3f(577.14f, 606.43f, 0.f);		
				glVertex3f(667.86f, 615.00f, 0.f);
				glVertex3f(668.57f, 610.71f, 0.f);
			
			glEnd();
		
			glBegin(GL_POLYGON);
		  
				glColor3ub(60,59,59);	
				glVertex3f(680.00f, 452.86f, 0.f);
				glVertex3f(686.43f, 448.57f, 0.f);
				glVertex3f(702.14f, 935.00f, 0.f);
				glVertex3f(695.00f, 932.14f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(71,70,70);	
				glVertex3f(686.43f, 448.57f, 0.f);
				glVertex3f(690.00f, 450.00f, 0.f);
				glVertex3f(705.00f, 933.57f, 0.f);
				glVertex3f(702.14f, 933.57f, 0.f);			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(71,70,70);	
				glVertex3f(705.00f, 921.43f, 0.f);
				glVertex3f(705.00f, 924.29f, 0.f);
				glVertex3f(824.29f, 918.57f, 0.f);
				glVertex3f(824.29f, 915.00f, 0.f);			
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(71,70,70);	
				glVertex3f(824.29f, 918.00f, 0.f);
				glVertex3f(825.00f, 926.43f, 0.f);
				glVertex3f(817.86f, 925.71f, 0.f);
				glVertex3f(818.57f, 918.29f, 0.f);			
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(106,73,41);	
				glVertex3f(824.14f, 915.00f, 0.f);
				glVertex3f(805.86f, 485.71f, 0.f);
				glVertex3f(690.00f, 464.29f, 0.f);
				glVertex3f(704.29f, 921.29f, 0.f);			
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(60,59,59);	
				glVertex3f(825.00f, 927.14f, 0.f);
				glVertex3f(827.14f, 926.43f, 0.f);
				glVertex3f(806.43f, 471.43f, 0.f);
				glVertex3f(803.57f, 470.71f, 0.f);			
			
			glEnd();
			
		//tembokputihlt3kiri
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(542.86f, 723.57f, 0.f);
				glVertex3f(547.86f, 1037.14f, 0.f);		
				glVertex3f(135.71f, 975.00f, 0.f);
				glVertex3f(140.00f, 743.57f, 0.f);
			
			glEnd();
			
			//pintult3
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(158,142,126);
				glVertex3f(468.57f, 749.29f, 0.f);
				glVertex3f(475.71f, 749.29f, 0.f);		
				glVertex3f(475.71f, 895.00f, 0.f);
				glVertex3f(469.29f, 895.71f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(132,119,106);
				glVertex3f(468.57f, 749.29f, 0.f);
				glVertex3f(462.14f, 754.29f, 0.f);		
				glVertex3f(462.14f, 894.29f, 0.f);
				glVertex3f(468.57f, 895.00f, 0.f);
				
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(132,119,106);	
				glVertex3f(462.14f, 894.29f, 0.f);
				glVertex3f(464.29f, 886.43f, 0.f);
				glVertex3f(307.14f, 878.57f, 0.f);
				glVertex3f(307.14f, 886.43f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(132,119,106);	
				glVertex3f(307.14f, 886.43f, 0.f);
				glVertex3f(311.43f, 887.14f, 0.f);
				glVertex3f(312.14f, 752.86f, 0.f);
				glVertex3f(305.00f, 754.29f, 0.f);
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(158,142,126);	
				glVertex3f(312.14f, 877.86f, 0.f);
				glVertex3f(317.86f, 877.86f, 0.f);
				glVertex3f(317.86f, 747.86f, 0.f);
				glVertex3f(311.43f, 752.14f, 0.f);
			glEnd();
			
				
			glBegin(GL_POLYGON);
		  
				glColor3ub(10,10,10);	
				glVertex3f(322.14f, 748.57f, 0.f);
				glVertex3f(462.14f, 750.71f, 0.f);
				glVertex3f(462.14f, 873.57f, 0.f);
				glVertex3f(323.57f, 868.57f, 0.f);
			
			glEnd();
			
			//kaca
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(106,177,177);	
				glVertex3f(329.29f, 752.86f, 0.f);
				glVertex3f(459.29f, 755.71f, 0.f);
				glVertex3f(460.00f, 865.71f, 0.f);
				glVertex3f(328.57f, 861.43f, 0.f);
			
			glEnd();
			
			//sekat1
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(10,10,10);	
				glVertex3f(355.71f, 753.57f, 0.f);
				glVertex3f(362.14f, 752.86f, 0.f);
				glVertex3f(362.86f, 865.00f, 0.f);
				glVertex3f(355.71f, 865.00f, 0.f);
			
			glEnd();
			
			//sekat2
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(10,10,10);	
				glVertex3f(389.29f, 755.71f, 0.f);
				glVertex3f(396.43f, 755.71f, 0.f);
				glVertex3f(396.43f, 865.71f, 0.f);
				glVertex3f(391.43f, 865.71f, 0.f);
			
			glEnd();
			
			//sekat3
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(10,10,10);	
				glVertex3f(427.14f, 757.86f, 0.f);
				glVertex3f(430.71f, 757.86f, 0.f);
				glVertex3f(430.71f, 867.86f, 0.f);
				glVertex3f(427.14f, 867.86f, 0.f);
			
			glEnd();
		
		//tiangabu2palingkirilt3
			glBegin(GL_POLYGON);
		  
				glColor3ub(155,155,155);
				glVertex3f(138.57f, 941.43f, 0.f);
				glVertex3f(127.86f, 939.29f, 0.f);		
				glVertex3f(130.71f, 770.00f, 0.f);
				glVertex3f(140.71f, 770.00f, 0.f);
			
			glEnd();
			
			//
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(-148.57f, 773.57f, 0.f);
				glVertex3f(143.57f, 777.86f, 0.f);		
				glVertex3f(144.29f, 737.86f, 0.f);
				glVertex3f(-147.14f, 729.29f, 0.f);
			
			glEnd();
		
		//temboktiangatasdepan
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(891.43f, 967.14f, 0.f);
				glVertex3f(891.43f, 975.71f, 0.f);		
				glVertex3f(544.29f, 1005.00f, 0.f);
				glVertex3f(544.29f, 995.71f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(102,102,102);
				glVertex3f(891.43f, 967.14f, 0.f);
				glVertex3f(859.29f, 962.86f, 0.f);		
				glVertex3f(547.86f, 987.86f, 0.f);
				glVertex3f(544.29f, 995.00f, 0.f);
			
			glEnd();
			
		//temboktiangataskiri
		
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);		
				glVertex3f(544.29f, 1005.00f, 0.f);
				glVertex3f(544.29f, 995.71f, 0.f);
				glVertex3f(91.43f,  940.71f, 0.f);
				glVertex3f(91.43f,  949.29f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(102,102,102);		
				glVertex3f(547.86f, 987.86f, 0.f);
				glVertex3f(544.29f, 995.00f, 0.f);
				glVertex3f(92.71f,  940.71f, 0.f);
				glVertex3f(125.71f, 937.14f, 0.f);
			
			glEnd();
			
		//tembokcoklattiangkanan
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(132,119,106);
				glVertex3f(562.86f, 422.14f, 0.f);
				glVertex3f(538.57f, 427.87f, 0.f);
				glVertex3f(549.29f, 957.86f, 0.f);
				glVertex3f(572.14f, 961.43f, 0.f);		
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(158,142,126);
				glVertex3f(562.86f, 422.14f, 0.f);
				glVertex3f(575.71f, 425.00f, 0.f);
				glVertex3f(586.43f, 962.14f, 0.f);
				glVertex3f(572.14f, 961.43f, 0.f);		
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(158,142,126);
				glVertex3f(572.14f, 941.43f, 0.f);
				glVertex3f(572.86f, 961.43f, 0.f);
				glVertex3f(846.43f, 944.29f, 0.f);
				glVertex3f(845.00f, 928.57f, 0.f);
						
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(39,35,31);
				glVertex3f(845.00f, 928.57f, 0.f);
				glVertex3f(820.71f, 925.71f, 0.f);		
				glVertex3f(585.71f, 937.86f, 0.f);
				glVertex3f(585.00f, 941.43f, 0.f);
			
			glEnd();
			

		//lantaiduadepan	
			glBegin(GL_POLYGON);
		  
				glColor3ub(106,73,41);
				glVertex3f(195.71f, 327.14f, 0.f);
				glVertex3f(537.86f,395.00f, 0.f);
				glVertex3f(542.86f,680.71f, 0.f);
				glVertex3f(192.14f,658.57f, 0.f);		
			
			glEnd();
			
		//batastembokdepanlt2
		
		//jendeladepan
		
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(188.57f,421.43f, 0.f);
				glVertex3f(510.71f,469.29f, 0.f);
				glVertex3f(512.14f,612.14f, 0.f);
				glVertex3f(185.00f,583.57f, 0.f);	
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(185.00f,583.57f, 0.f);
				glVertex3f(121.43f,596.43f, 0.f);
				glVertex3f(125.00f,443.57f, 0.f);
				glVertex3f(188.57f,420.71f, 0.f);	
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(137,137,137);
				glVertex3f(121.43f,596.43f, 0.f);
				glVertex3f(185.71f,582.14f, 0.f);
				glVertex3f(193.57f,586.43f, 0.f);
				glVertex3f(137.86f,599.29f, 0.f);			
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(137,137,137);
				glVertex3f(185.71f,582.14f, 0.f);
				glVertex3f(510.00f,612.86f, 0.f);
				glVertex3f(502.86f,615.00f, 0.f);
				glVertex3f(192.14f,586.43f, 0.f);			
			
			glEnd();
			
		
			glBegin(GL_POLYGON);
		  
				glColor3ub(155,155,155);
				glVertex3f(176.43f,601.43f, 0.f);
				glVertex3f(300.00f,613.57f, 0.f);
				glVertex3f(300.00f,631.43f, 0.f);
				glVertex3f(177.14f,622.14f, 0.f);	
			glEnd();
				

			
			glBegin(GL_POLYGON);
		  
				glColor3ub(96,96,96);
				glVertex3f(177.14f,622.14f, 0.f);
				glVertex3f(191.43f,628.57f, 0.f);
				glVertex3f(15.00f,659.29f, 0.f);
				glVertex3f(-16.43f,657.14f, 0.f);	
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(96,96,96);
				glVertex3f(191.43f,628.57f, 0.f);
				glVertex3f(280.00f,636.43f, 0.f);
				glVertex3f(300.00f,631.43f, 0.f);
				glVertex3f(177.14f,622.14f, 0.f);		
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(145.00f,454.29f, 0.f);
				glVertex3f(130.00f,450.71f, 0.f);
				glVertex3f(125.00f,586.43f, 0.f);
				glVertex3f(142.14f,583.57f, 0.f);		
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(137,137,137);
				glVertex3f(145.00f,454.29f, 0.f);
				glVertex3f(194.29f,435.71f, 0.f);
				glVertex3f(188.57f,429.29f, 0.f);
				glVertex3f(128.57f,450.71f, 0.f);		
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(158,142,126);
				glVertex3f(145.00f,454.29f, 0.f);
				glVertex3f(142.14f,583.57f, 0.f);
				glVertex3f(193.57f,572.86f, 0.f);
				glVertex3f(195.00f,436.43f, 0.f);		
			
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(158,142,126);
				glVertex3f(195.00f,436.43f, 0.f);
				glVertex3f(492.86f,480.71f, 0.f);
				glVertex3f(494.29f,602.86f, 0.f);		
				glVertex3f(193.57f,573.57f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(137,137,137);
				glVertex3f(188.57f,428.57f, 0.f);
				glVertex3f(502.86f,477.86f, 0.f);
				glVertex3f(492.14f,481.43f, 0.f);		
				glVertex3f(194.29f,436.43f, 0.f);
				
			glEnd();
			
			//kusendepanlt2
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(281.43f,459.29f, 0.f);
				glVertex3f(484.29f,489.29f, 0.f);
				glVertex3f(486.43f,595.71f, 0.f);		
				glVertex3f(280.71f,575.71f, 0.f);
				
			glEnd();
			
			//kacadepanlt2
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(126,211,211);
				glVertex3f(479.29f,492.14f, 0.f);
				glVertex3f(285.71f,465.00f, 0.f);
				glVertex3f(285.00f,569.29f, 0.f);		
				glVertex3f(481.29f,590.43f, 0.f);
				
			glEnd();
			
			//sekat1palingkiri
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(330.71f,467.14f, 0.f);
				glVertex3f(337.86f,467.14f, 0.f);
				glVertex3f(337.86f,575.00f, 0.f);		
				glVertex3f(330.71f,575.00f, 0.f);
				
			glEnd();
			
			//sekat2
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(384.29f,475.00f, 0.f);
				glVertex3f(388.57f,475.00f, 0.f);
				glVertex3f(388.57f,580.00f, 0.f);		
				glVertex3f(384.29, 580.00f, 0.f);
				
			glEnd();
			
			//sekat3
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(432.86f,482.86f, 0.f);
				glVertex3f(437.14f,482.14f, 0.f);
				glVertex3f(437.14f,587.86f, 0.f);		
				glVertex3f(432.86f,587.86f, 0.f);
				
			glEnd();
			
		//jendelalt2kiri
			glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(150.71f,460.71f, 0.f);
				glVertex3f(184.29f,449.29f, 0.f);
				glVertex3f(182.86f,568.57f, 0.f);		
				glVertex3f(150.00f,577.14f, 0.f);
				
			glEnd();
		
		//kaca
			glBegin(GL_POLYGON);
		  
				glColor3ub(106,177,177);
				glVertex3f(156.43f,466.43f, 0.f);
				glVertex3f(182.86f,456.43f, 0.f);
				glVertex3f(180.71f,563.57f, 0.f);		
				glVertex3f(155.00f,570.00f, 0.f);
				
			glEnd();
		
		//lt3tembokputihkecilbawah
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(548.57f,682.14f, 0.f);
				glVertex3f(548.57f,689.29f, 0.f);
				glVertex3f(187.14f,670.00f, 0.f);		
				glVertex3f(187.14f,658.57f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(96,96,96);
				glVertex3f(187.14f,667.86f, 0.f);
				glVertex3f(191.43f,670.71f, 0.f);
				glVertex3f(540.71f,691.43f, 0.f);		
				glVertex3f(548.57f,690.71f, 0.f);
				
			glEnd();
		//
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(187.14f,659.29f, 0.f);
				glVertex3f(191.43f,670.71f, 0.f);
				glVertex3f(-1.43f,695.71f, 0.f);		
				glVertex3f(-1.43f,687.86f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(96,96,96);
				glVertex3f(187.14f,667.86f, 0.f);
				glVertex3f(190.71f,670.71f, 0.f);
				glVertex3f(5.71f,696.43f, 0.f);
				glVertex3f(-1.43f,695.71f, 0.f);
				
			glEnd();
		
		//
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(-1.43f,687.14f, 0.f);
				glVertex3f(-1.43f,694.71f, 0.f);
				glVertex3f(-17.86f,694.57f, 0.f);
				glVertex3f(-17.86f,686.43f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				
				glVertex3f(-17.86f,686.43f, 0.f);
				glVertex3f(-17.86f,694.57f, 0.f);
				glVertex3f(-45.71f,697.86f, 0.f);
				glVertex3f(-45.71f,690.71f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(92,92,92);
				
				glVertex3f(-17.86f,694.57f, 0.f);
				glVertex3f(1.43f,696.43f, 0.f);
				glVertex3f(-30.71f,701.43f, 0.f);
				glVertex3f(-45.71f,697.86f, 0.f);
				
			glEnd();
		//
		
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				
				glVertex3f(-45.71f,702.14f, 0.f);
				glVertex3f(-46.43f,695.00f, 0.f);
				glVertex3f(-140.00f,707.14f, 0.f);
				glVertex3f(-140.00f,715.71f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(96,96,96);
				glVertex3f(-140.00f,715.71f, 0.f);
				glVertex3f(-135.71f,715.71f, 0.f);
				glVertex3f(-28.57f,700.71f, 0.f);
				glVertex3f(-34.29f,699.29f, 0.f);
				
			glEnd();
			
		//
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(-140.00f,708.57f, 0.f);
				glVertex3f(-135.71f,715.71f, 0.f);
				glVertex3f(-154.29f,714.29f, 0.f);
				glVertex3f(-155.00f,707.14f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(-155.00f,705.71f, 0.f);
				glVertex3f(-155.71f,715.14f, 0.f);
				glVertex3f(-174.29f,717.14f, 0.f);
				glVertex3f(-174.29f,709.29f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(96,96,96);
				glVertex3f(-155.00f,715.14f, 0.f);
				glVertex3f(-140.00f,713.57f, 0.f);
				glVertex3f(-146.43f,719.29f, 0.f);
				glVertex3f(-172.14f,717.86f, 0.f);
				
			glEnd();
		
		//lt2temboktengah
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(542.86f,692.86f, 0.f);
				glVertex3f(542.86f,752.86f, 0.f);
				glVertex3f(188.57f,742.86f, 0.f);
				glVertex3f(190.71f,670.00f, 0.f);
				
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(255,255,255);
				glVertex3f(550.00f,753.57f, 0.f);
				glVertex3f(550.71f,762.14f, 0.f);
				glVertex3f(185.00f,753.57f, 0.f);
				glVertex3f(185.71f,742.86f, 0.f);
				
			glEnd();

			
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(190.71f,670.00f, 0.f);
				glVertex3f(188.57f,742.86f, 0.f);				
				glVertex3f(-148.57f,765.00f, 0.f);
				glVertex3f(-147.14f,717.86f, 0.f);
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(185.00f,753.57f, 0.f);
				glVertex3f(185.71f,742.86f, 0.f);
				glVertex3f(-157.86f,757.86f, 0.f);
				glVertex3f(-157.86f,765.00f, 0.f);
			glEnd();
			
		 //jendela depan kiri
		 
		 //kusen
			glEnd();
			
				glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(339.64f, 121.79f, 0.f);
				glVertex3f(383.94f, 134.64f, 0.f);
				glVertex3f(383.94f, 348.57f, 0.f);
				glVertex3f(339.64f, 338.57f, 0.f);
				
					
			glEnd();
			
		 //kaca
			
			 glEnd();
			
				glBegin(GL_POLYGON);
		  
				glColor3ub(126,211,211);
				glVertex3f(343.45f, 129.80f, 0.f);
				glVertex3f(377.80f, 138.90f, 0.f);
				glVertex3f(377.80f, 342.95f, 0.f);
				glVertex3f(343.45f, 335.88f, 0.f);				
					
			glEnd();
		  
		//kusen
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(280.32f, 103.54f, 0.f);
				glVertex3f(324.76f, 118.19f, 0.f);
				glVertex3f(324.76f, 336.38f, 0.f);
				glVertex3f(280.32f, 328.30f, 0.f);				
					
			glEnd();
			
		//kaca
			glBegin(GL_POLYGON);
		  
				glColor3ub(126,211,211);
				glVertex3f(282.84f, 112.13f, 0.f);
				glVertex3f(318.70f, 122.23f, 0.f);
				glVertex3f(318.70f, 330.82f, 0.f);
				glVertex3f(282.84f, 324.26f, 0.f);				
					
			glEnd();

		//kusen    
			glBegin(GL_POLYGON);
		  
				glColor3ub(31,31,31);
				glVertex3f(224.25f, 87.88f, 0.f);
				glVertex3f(264.15f, 99.50f, 0.f);
				glVertex3f(264.15f, 323.75f, 0.f);
				glVertex3f(224.25f, 316.68f, 0.f);				
					
			glEnd();

		//kaca
			glBegin(GL_POLYGON);
		  
				glColor3ub(126,211,211);
				glVertex3f(224.25f, 94.95f, 0.f);
				glVertex3f(257.59f, 105.06f, 0.f);
				glVertex3f(257.59f, 318.280f, 0.f);
				glVertex3f(224.25f, 312.14f, 0.f);				
					
			glEnd();
		//batas jendela

			glBegin(GL_POLYGON);
		  
				glColor3ub(221,221,221);
				glVertex3f(145.46f, 60.68f, 0.f);
				glVertex3f(23.74f, 154.05f, 0.f);
				glVertex3f(16.16f, 369.72f, 0.f);
				glVertex3f(140.41f,309.11f, 0.f);				
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(155,155,155);
				glVertex3f(139.40f,310.40f, 0.f);
				glVertex3f(138.90f,318.70f, 0.f);
				glVertex3f(-40.41f,405.58f, 0.f);
				glVertex3f(-34.35f,394.46f, 0.f);				
					
			glEnd();
			
			glBegin(GL_POLYGON);
		  
				glColor3ub(155,155,155);
				glVertex3f(-34.35f,394.46f, 0.f);
				glVertex3f(-22.73f,144.96f, 0.f);
				glVertex3f(-30.81f,149.50f, 0.f);
				glVertex3f(-40.41f,405.58f, 0.f);				
					
			glEnd();
				
			glBegin(GL_POLYGON);
				
				glColor3ub(185,185,185);
				glVertex3f(-22.73f,144.96f, 0.f);
				glVertex3f(23.23f,155.06f, 0.f);
				glVertex3f(15.15f,369.72f, 0.f);
				glVertex3f(-34.35f,394.46f, 0.f);
			
			glEnd();

		//kaca kanan

		//kusen
			
			glBegin(GL_POLYGON);
				
				glColor3ub(22,22,22);
				glVertex3f(145.46f,101.02f, 0.f);
				glVertex3f(139.91f,309.11f, 0.f);
				glVertex3f(129.80f,313.15f, 0.f);
				glVertex3f(134.86f,105.56f, 0.f);
			
			glEnd();

		//kaca

			glBegin(GL_POLYGON);
				
				glColor3ub(106,177,177);
				glVertex3f(144.45f,108.09f, 0.f);
				glVertex3f(139.91f,307.59f, 0.f);
				glVertex3f(136.37f,309.61f, 0.f);
				glVertex3f(140.41f,109.10f, 0.f);
			
			glEnd();

		//kusen
			
			glBegin(GL_POLYGON);
				
				glColor3ub(22,22,22);
				glVertex3f(128.29f,113.14f, 0.f);
				glVertex3f(122.72f,319.21f, 0.f);
				glVertex3f(96.97f,330.32f, 0.f);
				glVertex3f(101.52f,131.82f, 0.f);
			
			glEnd();

		//kaca
			glBegin(GL_POLYGON);
				
				glColor3ub(106,177,177);
				glVertex3f(125.76f,123.24f, 0.f);
				glVertex3f(119.70f,318.20f, 0.f);
				glVertex3f(104.05f,326.28f, 0.f);
				glVertex3f(108.85f,133.85f, 0.f);
			
			glEnd();
			
		//kusen
			
			glBegin(GL_POLYGON);
				
				glColor3ub(22,22,22);
				glVertex3f(94.95f,136.37f, 0.f);
				glVertex3f(87.88f,333.35f, 0.f);
				glVertex3f(63.13f,346.48f, 0.f);
				glVertex3f(69.20f,153.04f, 0.f);
			
			glEnd();

		//kaca
			
			glBegin(GL_POLYGON);
				
				glColor3ub(106,177,177);
				glVertex3f(92.43f,147.48f, 0.f);
				glVertex3f(87.38f,335.88f, 0.f);
				glVertex3f(70.71f,341.94f, 0.f);
				glVertex3f(75.76f,157.09f, 0.f);
			
			glEnd();
			
			//batasjendela
			
			glBegin(GL_POLYGON);
				
				glColor3ub(96,96,96);
				glVertex3f(140.50f,313.65f, 0.f);
				glVertex3f(424.77f,368.20f, 0.f);
				glVertex3f(420.22f,370.22f, 0.f);
				glVertex3f(144.45f,317.19f, 0.f);
			
			glEnd();
				
			glBegin(GL_POLYGON);
				
				glColor3ub(185,185,185);
				glVertex3f(220.21f,318.20f, 0.f);
				glVertex3f(426.28f,359.11f, 0.f);
				glVertex3f(424.77f,368.20f, 0.f);
				glVertex3f(219.71f,327.29f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(96,96,96);
				glVertex3f(149.50f,314.66f, 0.f);
				glVertex3f(204.05f,324.76f, 0.f);
				glVertex3f(5.56f,414.67f, 0.f);
				glVertex3f(-40.91f,405.58f, 0.f);
			
			glEnd();

			

		//pintu
			
		//tembokhalaman
			
			glBegin(GL_POLYGON);
				
				glColor3ub(155,155,155);
				glVertex3f(557.10f,118.19f, 0.f);
				glVertex3f(468.21f,175.77f, 0.f);
				glVertex3f(468.21f,361.63f, 0.f);
				glVertex3f(557.10f,380.32f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(255,255,255);
				glVertex3f(556.59f,120.21f, 0.f);
				glVertex3f(840.47f,210.11f, 0.f);
				glVertex3f(840.47f,464.17f, 0.f);
				glVertex3f(556.59f,409.62f, 0.f);
			
			glEnd();
			
			
			glBegin(GL_POLYGON);
				
				glColor3ub(155,155,155);
				glVertex3f(556.59f,409.62f, 0.f);
				glVertex3f(556.59,394.97f, 0.f);
				glVertex3f(538.41f,401.05f, 0.f);
				glVertex3f(538.41f,416.18f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(221,221,221);
				glVertex3f(532.86f,417.86f, 0.f);
				glVertex3f(561.43f,410.00f, 0.f);
				glVertex3f(561.43f,415.71f, 0.f);
				glVertex3f(532.86f,425.00f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(255,255,255);
				glVertex3f(561.43f,410.00f, 0.f);
				glVertex3f(561.43f,415.71f, 0.f);
				glVertex3f(865.71f,474.291f, 0.f);
				glVertex3f(865.71f,465.71f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(137,137,137);
				glVertex3f(865.71f,474.291f, 0.f);
				glVertex3f(830.00f,480.71f, 0.f);
				glVertex3f(533.57f,425.71f, 0.f);
				glVertex3f(561.43f,417.14f, 0.f);
				
			glEnd();
			
			
			glBegin(GL_POLYGON);
				
				glColor3ub(99,98,83);
				glVertex3f(570.23f,154.05f, 0.f);
				glVertex3f(685.39f,188.90f, 0.f);
				glVertex3f(661.14f,198.49f, 0.f);
				glVertex3f(570.23f,170.72f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(185,185,185);
				glVertex3f(685.39f,188.90f, 0.f);
				glVertex3f(661.14f,198.49f, 0.f);
				glVertex3f(661.14f,390.93f, 0.f);
				glVertex3f(685.39f,396.48f, 0.f);
				
			glEnd();
			
			//jendelakanan
			//kusen
			glBegin(GL_POLYGON);
				
				glColor3ub(31,31,31);
				glVertex3f(661.14f,198.49f, 0.f);
				glVertex3f(661.14f,390.93f, 0.f);
				glVertex3f(573.77f,371.23f, 0.f);
				glVertex3f(570.23f,170.72f, 0.f);
				
			glEnd();
			
			//kacakanan
			glBegin(GL_POLYGON);
				
				glColor3ub(126,211,211);
				glVertex3f(608.11f,187.89f, 0.f);
				glVertex3f(655.59f,203.04f, 0.f);
				glVertex3f(655.59f,352.54f, 0.f);
				glVertex3f(611.65f,341.94f, 0.f);
				
			glEnd();
			
			//kacakiri
			glBegin(GL_POLYGON);
				
				glColor3ub(126,211,211);
				glVertex3f(602.05f,187.38f, 0.f);
				glVertex3f(605.59f,340.42f, 0.f);
				glVertex3f(574.23f,333.86f, 0.f);
				glVertex3f(570.23f,177.79f, 0.f);
				
			glEnd();
			
			//kacaatas
			
			glBegin(GL_POLYGON);
				
				glColor3ub(126,211,211);
				glVertex3f(660.64f,359.61f, 0.f);
				glVertex3f(660.64f,389.41f, 0.f);
				glVertex3f(574.78f,370.22f, 0.f);
				glVertex3f(574.78f,339.41f, 0.f);
				
			glEnd();
			
			//lantai teras
			glBegin(GL_POLYGON);
				
				glColor3ub(137,137,137);
				glVertex3f(416.13f,142.94f, 0.f);
				glVertex3f(466.69f,128.79f, 0.f);
				glVertex3f(544.98f,151.52f, 0.f);
				glVertex3f(466.69f,186.88f, 0.f);
				glVertex3f(416.65f,170.72f, 0.f);
			glEnd();
			

		//pintu
			
			
			glBegin(GL_POLYGON);
				
				glColor3ub(14,14,14);
				glVertex3f(416.13f,182.33f, 0.f);
				glVertex3f(420.73f,185.36f, 0.f);
				glVertex3f(420.73f,342.44f, 0.f);
				glVertex3f(416.13f,346.99f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(126,211,211);
				glVertex3f(415.17f,188.90f, 0.f);
				glVertex3f(419.72f,189.91f, 0.f);
				glVertex3f(419.72f,342.44f, 0.f);
				glVertex3f(415.17f,341.94f, 0.f);
			
			glEnd();
			
			
			//tiang hitam
			
			//anaktanggapertama
			glBegin(GL_POLYGON);
				
				glColor3ub(137,137,137);
				glVertex3f(472.75f,86.87f, 0.f);
				glVertex3f(573.77f,120.71f, 0.f);
				glVertex3f(559.77f,126.77f, 0.f);
				glVertex3f(467.20f,97.98f, 0.f);
				glVertex3f(466.69f,89.90f, 0.f);
			
			glEnd();
			//batasanaktanggasisanyakebawahtiang
			
			
			glBegin(GL_POLYGON);
				
				glColor3ub(60,59,59);
				glVertex3f(396.48f,137.38f, 0.f);
				glVertex3f(396.48f,105.56f, 0.f);
				glVertex3f(452.55f,74.75f, 0.f);
				glVertex3f(452.55f,111.12f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(37,37,37);
				glVertex3f(452.55f,111.12f, 0.f);
				glVertex3f(396.48f,137.38f, 0.f);
				glVertex3f(411.13f,141.93f, 0.f);
				glVertex3f(466.69f,116.17f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(71,70,70);
				glVertex3f(452.55f,74.75f, 0.f);
				glVertex3f(468.71f,79.80f, 0.f);
				glVertex3f(468.71f,370.73f, 0.f);
				glVertex3f(452.55f,367.19f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(57,56,56);
				glVertex3f(452.55f,110.61f, 0.f);
				glVertex3f(452.55f,367.70f, 0.f);
				glVertex3f(421.23f,379.31f, 0.f);
				glVertex3f(419.21f,127.78f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(71,70,70);
				glVertex3f(442.45f,116.17f, 0.f);
				glVertex3f(442.45f,335.88f, 0.f);
				glVertex3f(431.84f,341.94f, 0.f);
				glVertex3f(431.84f,121.72f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(37,37,37);
				glVertex3f(431.84f,121.12f, 0.f);
				glVertex3f(441.94f,115.66f, 0.f);
				glVertex3f(441.94f,124.25f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(37,37,37);
				glVertex3f(456.55f,367.19f, 0.f);
				glVertex3f(704.08f,416.69f, 0.f);
				glVertex3f(676.80f,426.79f, 0.f);
				glVertex3f(421.74f,379.82f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(71,70,70);
				glVertex3f(455.58f,359.61f, 0.f);
				glVertex3f(704.08f,409.62f, 0.f);
				glVertex3f(704.08f,416.69f, 0.f);
				glVertex3f(455.58f,367.70f, 0.f);
			
			glEnd();
			

			
			//tangga
			
			glBegin(GL_POLYGON);
				
				glColor3ub(255,255,255);
				glVertex3f(468.71f,118.69f, 0.f);
				glVertex3f(544.98f,142.94f, 0.f);
				glVertex3f(544.98f,151.52f, 0.f);
				glVertex3f(468.71f,129.50f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(137,137,137);
				glVertex3f(468.71f,107.58f, 0.f);
				glVertex3f(559.12f,135.36f, 0.f);
				glVertex3f(544.98f,142.94f, 0.f);
				glVertex3f(468.71f,118.69f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(255,255,255);
				glVertex3f(468.71f,96.97f, 0.f);
				glVertex3f(559.12f,127.28f, 0.f);
				glVertex3f(559.12f,134.35f, 0.f);
				glVertex3f(468.71f,107.57f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(255,255,255);
				glVertex3f(472.75f,86.87f, 0.f);
				glVertex3f(573.77f,120.71f, 0.f);
				glVertex3f(573.77f,110.11f, 0.f);
				glVertex3f(472.75f,77.78f, 0.f);
			
			glEnd();
			
			glBegin(GL_POLYGON);
				
				glColor3ub(221,221,221);
				glVertex3f(472.75f,86.87f, 0.f);
				glVertex3f(467.20f,90.41f, 0.f);
				glVertex3f(467.20f,79.30f, 0.f);
				glVertex3f(472.75f,77.78f, 0.f);
			
			glEnd();
			
	glPopMatrix();

			


}


int main(void)
{
	GLFWwindow* window;
	if (!glfwInit()) exit(EXIT_FAILURE);

	window = glfwCreateWindow(2024, 3040, "Miqdad", NULL, NULL);

	if (!window)
	{
    	glfwTerminate();
    	exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	glfwSetKeyCallback(window, key_callback);

	while (!glfwWindowShouldClose(window))
	{
    	setup_viewport(window);

    	display();

    	glfwSwapBuffers(window);
    	glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}
